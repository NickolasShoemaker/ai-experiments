/**************************************************************
* Nick Shoemaker                                              *
*                                                             *
* This is a very simple pattern learning neural network that  *
* uses learning rate and momentum.                            *
*                                                             *
***************************************************************/

#ifndef NEURALNET_H
#define NEURALNET_H

#include <iostream>
#include <random>
#include <functional>
#include <complex>

//create random number generator and bind it to an output range
std::default_random_engine generator(0);
std::uniform_real_distribution<double> input_weight_distribution(-0.2, 0.2);
std::uniform_real_distribution<double> output_weight_distribution(-2.0, 2.0);
auto input_weight_random_range = std::bind(input_weight_distribution, generator);
auto output_weight_random_range = std::bind(output_weight_distribution, generator);

//aliases for ease of use
using matrix=std::vector<std::vector<double>>;
using pattern=std::pair<std::vector<double>, std::vector<double>>;
using patterns=std::vector<pattern>;
using nodes=std::vector<double>;

class neuralnet{
    unsigned int number_input_nodes;
    unsigned int number_hidden_nodes;
    unsigned int number_output_nodes;

    nodes input_nodes;
    nodes hidden_nodes;
    nodes output_nodes;

    matrix input_weight_matrix;
    matrix output_weight_matrix;

    matrix input_momentum_matrix;
    matrix output_momentum_matrix;

    void set_matrix_size(matrix& mat, uint len, uint wid){
        mat.resize(len);
        for(int i = 0; i < len; i++){
            mat[i].resize(wid);
        }
    }
    //sigmoid function to get value of nodes
    inline double sigmoid(double x){
        return tanh(x);
    }
    //derivative sigmoid
    inline double dsigmoid(double x){ return (1.0 - pow(x,2)); };

    void print_pattern(nodes nd){
        std::cout << "[ ";
        for(auto& n: nd){
            std::cout << n << ", ";
        }
        std::cout << '\b' << '\b' << " ]";
    }
public:
    /*
    * This constructs a tri-layer neural network.
    * ni: The number of inputs our patterns will have
    * no: The number of outputs our patterns will have
    * nh: The number of hidden nodes between our input and output.
    *     This is usually the same as ni.
    */
    neuralnet(uint ni ,uint nh ,uint no){
        number_input_nodes = ni + 1; //we add a bias node  + 1
        number_hidden_nodes = nh;
        number_output_nodes = no;

        //set the array of nodes to 1.0
        input_nodes.resize(number_input_nodes, 1.0);
        hidden_nodes.resize(nh, 1.0);
        output_nodes.resize(no, 1.0);

        //init weight matrix
        set_matrix_size(input_weight_matrix, number_input_nodes, nh);
        set_matrix_size(output_weight_matrix, nh, no);
        for(int i = 0; i < number_input_nodes; i++){
            for(int j = 0; j < nh; j++){
                input_weight_matrix[i][j] = input_weight_random_range();
            }
        }
        for(int i = 0; i < nh; i++){
            for(int j = 0; j < no; j++){
                output_weight_matrix[i][j] = output_weight_random_range();
            }
        }

        //set weight momentum, this makes learning faster for repeated
        //concepts
        set_matrix_size(input_momentum_matrix, number_input_nodes, nh);
        set_matrix_size(output_momentum_matrix, nh, no);
        for(int i = 0; i < number_input_nodes; i++){
            for(int j = 0; j < nh; j++){
                input_momentum_matrix[i][j] = 0;
                }
        }
        for(int i = 0; i < nh; i++){
            for(int j = 0; j < no; j++){
                output_momentum_matrix[i][j] = 0;
            }
        }
    }

    /*
    * Calculates the new values for all of the nodes based on the provided
    * input and the current weights
    */
    nodes update(nodes& nd){
        //we can not use the wrong number of inputs or else the algorithm breaks
        if((number_input_nodes - 1) != nd.size())
            throw(std::string("Error: Incorrect number of inputs for network of this size\n"));

        //set input nodes
        for(int i = 0; i < (number_input_nodes - 1); i++)
            input_nodes.at(i) = nd.at(i);

        //set bias node
        input_nodes.at(number_input_nodes - 1) = 0.0;

        //set hidden nodes by sum of all the input nodes and their wights
        //there is a weight for every node to node matching. This is what
        //gives the neural net its structure
        for(int i = 0; i < number_hidden_nodes; i++){
            double sum = 0.0;
            for(int j = 0; j < number_input_nodes; j++)
                sum += input_nodes.at(j) * input_weight_matrix[j][i];
            hidden_nodes.at(i) = sigmoid(sum);
        }

        //We do the weights for the output layer, similar to above
        for(int i = 0; i < number_output_nodes; i++){
            double sum = 0.0;
            for(int j = 0; j < number_hidden_nodes; j++)
                sum += hidden_nodes.at(j) * output_weight_matrix[j][i];
            output_nodes.at(i) = sigmoid(sum);
        }

        //Whatever our output nodes end up as is our output
        return output_nodes;
    }
    /*
    * Reset weights and other internal values to represent what we learned
    */
    double backpropogate(nodes expected_outputs, double learn_rate, double momentum){
        if(number_output_nodes != expected_outputs.size())
            throw(std::string("Error:Number of outputs doesn't match number of output nodes\n"));

        //These are some constructs to help us calculate error
        //at the different layers
        nodes output_delta(number_output_nodes, 0.0);
        nodes hidden_delta(number_hidden_nodes, 0.0);

        //output error
        for(int i = 0; i < number_output_nodes; i++){
            output_delta.at(i) = dsigmoid(output_nodes.at(i)) * (expected_outputs.at(i) - output_nodes.at(i));
        }

        //hidden error
        for(int i = 0; i < number_hidden_nodes; i++){
            double error = 0.0;
            for(int j = 0; j < number_output_nodes; j++){
                error += output_delta.at(j) * output_weight_matrix[i][j];
            }
            hidden_delta.at(i) = dsigmoid(hidden_nodes.at(i)) * error;
        }

        //update output weights
        for(int i = 0; i < number_hidden_nodes; i++){
            for(int j = 0; j < number_output_nodes; j++){
                double change = output_delta.at(j) * hidden_nodes.at(i);
                output_weight_matrix[i][j] += (learn_rate * change) + (momentum * output_momentum_matrix[i][j]);
                output_momentum_matrix[i][j] = change;
            }
        }

        //update input weights
        for(int i = 0; i < number_input_nodes; i++){
            for(int j = 0; j < number_hidden_nodes; j++){
                double change = hidden_delta.at(j) * input_nodes.at(i);
                input_weight_matrix[i][j] += (learn_rate * change) + (momentum * input_momentum_matrix[i][j]);
                input_momentum_matrix[i][j] = change;
            }
        }

        //return total error from expected values
        double total_error = 0.0;
        for(int i = 0; i < number_output_nodes; i++){
            total_error += 0.5 * pow((expected_outputs.at(i) - output_nodes.at(i)), 2);
        }
        return total_error;
    }
    /*
    * Calculate and print input to output mappings without changing internal
    * values
    */
    void test(patterns pat){
        for(auto& p : pat){
            print_pattern(p.first);
            std::cout << " -> ";
            try{
                print_pattern(update(p.first));
            }catch(std::string& str){
                std::cout << str;
                exit(1);
            }catch(std::out_of_range& e){
                std::cout << "test: update caused an " << e.what() << std::endl;
                exit(1);
            }
            std::cout << std::endl;
        }
    }
    /*
    * Calculate and print input to output mappings changing internal
    * values and updating the neural network
    */
    void train(patterns pat, int iterations=1000, double learnumber_input_nodesng_rate=0.5, double momentum_factor=0.1){
        for(int i=0; i < iterations; i++){
            double error = 0.0;
            for(auto& p: pat){
                try{
                update(p.first);
                error = error + backpropogate(p.second, learnumber_input_nodesng_rate, momentum_factor);
                //std::cout << "error:" << error << std::endl;
                }catch(std::string exception){
                    std::cout << exception;
                    exit(1);
                }
            }
        }
    }
    /*
    * Print out network information for debugging
    */
    void debug_print(){
        std::cout << "Input weight matrix\n";
        for(int i = 0; i < number_input_nodes; i++){
            for(int j = 0; j < number_hidden_nodes; j++){
                std::cout << input_weight_matrix[i][j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout << "\nOutput weight matrix\n";
        for(int i = 0; i < number_hidden_nodes; i++){
            for(int j = 0; j < number_output_nodes; j++){
                std::cout << output_weight_matrix[i][j] << " ";
            }
            std::cout << std::endl;
        }
    }
};

#endif //NEURALNET_H