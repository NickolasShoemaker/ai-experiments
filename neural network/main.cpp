/******************************************************
* Nick Shoemaker                                      *
*                                                     *
* This program tests the neural net class using some  *
* classic examples of AI.                             *
*                                                     *
******************************************************/

#include "neuralnet.h"

int main(int argc, char const *argv[]){

    //The xor problem is a classic
    //AI problem as xor is the simplest of computer operations
    neuralnet nn(2,2,1);
    patterns classic_xor = {
        {{0.0, 0.0}, {0.0}},
        {{0.0, 1.0}, {1.0}},
        {{1.0, 0.0}, {1.0}},
        {{1.0, 1.0}, {0.0}}
    };

    nn.train(classic_xor);
    nn.test(classic_xor);
    std::cout << std::endl;
    nn.debug_print();

    //we can also learn 1 to 1 mappings
    //thus we can learn pixels of photos and more interesting things
    neuralnet picture_match_net(4, 4, 4);
    patterns simple_bitmap = {
       {{1,0,1,0},{1,0,1,0}},
       {{0,1,0,1},{0,1,0,1}},
       {{1,1,1,0},{1,1,1,0}},
       {{1,1,1,1},{1,1,1,1}}
    };

    picture_match_net.train(simple_bitmap);
    picture_match_net.test(simple_bitmap);
    std::cout << std::endl;
    nn.debug_print();

    //A slightly more complex problem is the childrens game
    //rock paper scisors, We can use output ranges to get numbers
    //other than 0 to 1, in this case we use 0.5 as 1 and 1 as 2
    //we could just multiply the output by 2 if we needed it elsewhere.
    // 0     1       2
    patterns rock_paper_scisors = {
        {{0,0}, {0}},
        {{0,1}, {0.5}},
        {{0,2}, {0}},
        {{1,0}, {0.5}},
        {{1,1}, {0.5}},
        {{1,2}, {1}},
        {{2,0}, {0}},
        {{2,1}, {1}},
        {{2,2}, {1}}
    };

    neuralnet nn1(2, 2, 1);
    nn1.train(rock_paper_scisors, 2000);
    nn1.test(rock_paper_scisors);
    std::cout << std::endl;
    nn.debug_print();

    return 0;
}