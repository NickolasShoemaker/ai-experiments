/*************************************************************************************
* Nick Shoemaker                                                                     *
*                                                                                    *
* This is a Hello World Genetic algorithm. The goal is to print Hello World to the   *
* screen or at least something as close as possible to it. For this to work we use   *
* strings as our organisms with chars as our genes.                                  *
*                                                                                    *
* There are flags to play with as well.                                              *
*************************************************************************************/

#include <random>
#include <string>
#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <chrono>

/*
* Our organisms are simply a genome and a fitness score
*/
struct organism{
	int fitness;
	std::string genome;
	organism(std::string g = "", int f = 0){
		fitness = f;
		genome = g;
	}
};

/*
* c++11 alias for brevity
*/
using gene = char;
using population = std::vector<organism>;

/*
* This is a helper function to get a valid gene. We are only interested in
* Askii range 32-126 a.k.a the printable characters.
*/
std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
std::uniform_int_distribution<gene> distribution(' ','~');
auto random_gene = std::bind(distribution, generator);

/*
* This is our target, the most perfect form of our organisms.
* This is the major downfall of these types of algorithms in that
* you must be able to know what you are looking for or at the very
* least tell if you are getting closer.
*/
organism target("Hello World!");

/*
* Debug function to see what the population looks like
*/
void print_population(population pop, int size){
	std::cout << "\n\nPOPULATION----------------\n";
	for(int i = 0; i < size; i++){
		std::cout << "organism " << i << " is " << pop[i].genome << " {" << pop[i].fitness << "}"<< std::endl;
	}
}

/*
* An important point to consider is how we will initialize our organisms or
* in other words populate our community. This brings us back to our old friend
* the cold start problem. To ensure we have a good amount of genetic diversity
* we used controlled random to generate our population.
*/
population populate(int size){
	population pop(size);
	for(int i = 0; i < size; i++){
		for(int j = 0; j < target.genome.size(); j++){
			pop[i].genome.append(1,random_gene());
		}
	}

	return std::move(pop);
}

/*
* Crossover is one of the simplest and yet effective breeding algorithms.
* Two genomes are sliced at some point and a new organism is made by
* combining the first part of one and the second of the other.
*
* A better algorithm would keep track of dominant and recessive and try and
* be more predictive.
*/
organism crossover(organism a,organism b){
	organism baby;
	int cut = generator() % target.genome.size();	//get a place to cut on
	for(int i = 0; i < cut; i++)
		baby.genome.append(1,a.genome[i]);
	for(int i = cut;i < target.genome.size(); i++)
		baby.genome.append(1,b.genome[i]);
	return std::move(baby);
}

/*
* We run into a problem by only populating and breeding. We are limited to the
* genetic variation of our initial population and therefor may end up in the 
* situation where we can not get closer to our target. To counter this we have
* mutation. At some interval we can mutate a gene in our population. If the gene
* is fit it will be breed in and stay otherwise it will die off.
*/
gene mutate(gene g){
	if(g < '~')
		return g++;
	else
		return random_gene();
}

/*
* linear matting pattern. Very simple but limits genetic variation.
*/
population mate(population pop, float mutation){
	population next_gen(pop.size());

	//breed until we fill up again starting from the top
	//We have an elitism of one, that means we always keep the 1 best from each generation
	next_gen[0] = pop[0];

	int current_org = -1;
	for(int i = 1; i < pop.size(); i++){
		next_gen[i] = (crossover(pop[(current_org + i) % pop.size()], pop[(current_org + i + 1) % pop.size()]));
		current_org + 1;

		if(generator() % 100 < mutation * 100){
			int offset = generator() % target.genome.size();
			next_gen[i].genome.at(offset) = mutate(pop[i].genome.at(offset));
		}
	}
	return std::move(next_gen);
}

/*
* random matting pattern. Increases genetic variation.
*/
population rand_mate(population pop, float mutation){
	population next_gen(pop.size());

	//breed until we fill up again starting from the top
	//We have an elitism of one, that means we always keep the 1 best from each generation
	next_gen[0] = pop[0];

	for(int i = 1; i < pop.size(); i++){
		next_gen[i] = (crossover(pop[generator() % pop.size()], pop[generator() % pop.size()]));

		if(generator() % 100 < mutation * 100){
			int offset = generator() % target.genome.size();
			next_gen[i].genome.at(offset) = mutate(pop[i].genome.at(offset));
		}
	}
	return std::move(next_gen);
}

/*
* This is the most important point of the entire system. The heuristic we
* use to determine what organisms should survive and breed and which should
* die off. In our simple example we just calculate the sum of the distances
* each gene is from the target organism's gene
*/
int fitness(organism& org){
	int total = 0;
	for(int i = 0; i < target.genome.size(); i++){
		int temp = (int)(org.genome[i] - target.genome[i]);
		if(temp < 0)
			temp *= -1;	//lazy abs()
		total += temp;
	}
	org.fitness = total;
	return total;
}

/*
* Usage message
*/
void usage(const char* program){
	std::cout << program << " <population size> <generations> [error] [mutation rate]\n";
	std::cout << "\tpopulation size:\t number of organisms in each generation\n";
	std::cout << "\tgenerations:\t\t maximum number of generations\n";
	std::cout << "\terror:\t\t\t how low percent error gets before stopping, default .15\n";
	std::cout << "\tmutation rate:\t\t how often to mutate an organism, default .07\n";
}

/*
* Main function
*/
int main(int argc, char const **argv){

	unsigned int size;
	unsigned int generations;
	float error;
	float mutation;

	if(argc == 5){
		size = std::stoi(argv[1]);
		generations = std::stoi(argv[2]);
		error = std::stof(argv[3]);
		mutation = std::stof(argv[4]);
	}else if(argc == 3){
		size = std::stoi(argv[1]);
		generations = std::stoi(argv[2]);
		error = .15;
		mutation = .07;
	}else{
		usage(argv[0]);
		exit(1);
	}

	population pop = populate(size);
	unsigned best = 0xFFFFFFFF;
	int gen = 0;

	//natural selection, calculate and sort by fitness
	while(gen < generations && (best / 100.0) > error){
		for(int i = 0; i < size; i++)
			fitness(pop[i]);

		std::sort(pop.begin(), pop.end(), [](const organism a,const organism b){return a.fitness < b.fitness;});

		if(pop[0].fitness < best){
			best = pop[0].fitness;
			std::cout << "At generation " << gen << " Best: " << pop[0].genome << " {" << pop[0].fitness << "}" << std::endl;
		}
	
		pop = rand_mate(pop, mutation);
		gen++;
	}

	//Print result of last generation
	std::cout << "At generation " << gen << " Best: " << pop[0].genome << " {" << pop[0].fitness << "}" << std::endl;

	return 0;
}