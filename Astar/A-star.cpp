/*************************************************************************************
* Nick Shoemaker                                                                     *
*                                                                                    *
* This is an implementation of the A* search algorithm for a 2 Dimentional graph.    *
* A* is commonly used for unit pathing in RTS games. This is a good example of       *
* simulated artifictial intelligence a.k.a it is intelligent if it can be mistaken   *
* as intelligent.                                                                    *
*                                                                                    *
*************************************************************************************/
#include "A-star.h"

#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdlib>

/*
* Return all of the adjacent nodes that are not wall and are in the graph
*/
template<typename T>
std::vector<star_node*> get_adjacent(graph2d<T> g, star_node* node){
	std::vector<star_node*> neighbors;
	star_node* temp;

	//there are always 8 adjacent nodes in a 2D grid
	if(node->x + 1 < g.width){
		if(node->y + 1 < g.height){
			temp = g.get(node->x+1, node->y+1);
			if(temp->type != wall)
				neighbors.push_back(temp);
		}
		temp = g.get(node->x+1, node->y);
		if(temp->type != wall)
			neighbors.push_back(temp);
		if(node->y - 1 >= 0){
			temp = g.get(node->x+1, node->y-1);
			if(temp->type != wall)
				neighbors.push_back(temp);
		}
	}

	if(node->y + 1 < g.height){
		temp = g.get(node->x, node->y+1);
		if(temp->type != wall)
			neighbors.push_back(temp);
	}
	if(node->y - 1 >= 0){
		temp = g.get(node->x, node->y-1);
		if(temp->type != wall)
			neighbors.push_back(temp);
	}

	if(node->x - 1 >= 0){
		if(node->y + 1 < g.height){
			temp = g.get(node->x-1, node->y+1);
			if(temp->type != wall)
				neighbors.push_back(temp);
		}
		temp = g.get(node->x-1, node->y);
		if(temp->type != wall)
				neighbors.push_back(temp);
		if(node->y - 1 >= 0){
			temp = g.get(node->x-1, node->y-1);
			if(temp->type != wall)
				neighbors.push_back(temp);
		}
	}

	return std::move(neighbors);
}

/*
* Trace back the best path from end to start
*/
std::vector<star_node*> path(star_node* from){
	std::vector<star_node*> total_path;

	star_node* current = from;
	while(current != nullptr){
		total_path.push_back(current);
		current = current->parent;
	}

	return std::move(total_path);
}

/*
* strait line heuristic, very simple but not efficient
*/
void set_hueristic(star_node* from, star_node* to){
	from->hueristic = abs(from->x - to->x) * 10 + abs(from->y - to->y) * 10;
}

/*
*  get cost from start to here
*/
void set_cost(star_node* node){
	//the cost of diagonal movement is 4 more
	if(	node->x == node->parent->x + 1 && node->y == node->parent->y + 1 ||
		node->x == node->parent->x - 1 && node->y == node->parent->y + 1 ||
		node->x == node->parent->x + 1 && node->y == node->parent->y - 1 ||
		node->x == node->parent->x - 1 && node->y == node->parent->y - 1){
		node->cost = node->parent->cost + 14;
	}else{
		node->cost = node->parent->cost + 10;
	}
}

/*
* A*(graph, start, end)
*/
template<typename T>
std::vector<star_node*> Astar(graph2d<T> graph, star_node* start, star_node* target){
	std::vector<star_node*> open;	//open list
	set_open_list(open);
	start->parent = nullptr;
	set_hueristic(start, target);
	push_list(open, start);
	std::vector<star_node*> closed;	//closed list

	star_node* current = start;

	while(!open.empty() && current != target){
		current = pop_list(open);

		graph.print();
		std::cout << "\n\n";

		//wait for user so they can step through
		char dummy;
		std::cin >> dummy; 

		closed.push_back(current);
		for(auto& neighbor: get_adjacent(graph, current)){
			if(std::find(closed.begin(), closed.end(), neighbor) == closed.end()){	//not in closed list
				if(std::find(open.begin(), open.end(), neighbor) == open.end()){	//not in open list
					neighbor->parent = current;
					set_cost(neighbor);
					set_hueristic(neighbor, target);
					neighbor->set_score();

					push_list(open, neighbor);
				}else{//in open list
					//if this path is better update to use this parent
					float save_cost = neighbor->cost;
					star_node* save_node = neighbor->parent;

					neighbor->parent = current;
					set_cost(neighbor);
					if(save_cost < neighbor->cost){ //switch back
						neighbor->cost = save_cost;
						neighbor->parent = save_node;
					}else{
						neighbor->set_score();
						update_list(open);
					}
				}
			}
		}
	}

	return path(current);
}

/*
* main
*/
int main(int argc, char const *argv[]){
	graph2d<star_node> graph(10, 10);
	star_node* start = graph.get(3, 5);
	start->type = important;
	star_node* end = graph.get(7, 5);
	end->type = important;

	star_node* wall2 = graph.get(5, 4);
	wall2->type = wall;
	star_node* wall3 = graph.get(5, 5);
	wall3->type = wall;
	star_node* wall4 = graph.get(5, 6);
	wall4->type = wall;

	auto paths = Astar(graph, start, end);
	std::cout << "Final path is ";
	for(auto& n: paths){
		std::cout << "(" << n->x << "," << n->y << "), ";
	}
	std::cout << std::endl;

	return 0;
}