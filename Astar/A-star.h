#ifndef A_STAR_H
#define A_STAR_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>

//Define infinity as very large
constexpr int infinity = 0xFFFFFFFF;

/*
* We want to treat certain nodes differently
*/
enum node_type{
	empty,
	wall,
	important,
};

//Map nodes
struct star_node{
	float total;		//f value = f + g
	float hueristic;	//h value
	float cost;			//g value
	node_type type;
	int x;
	int y;
	star_node* parent;

	star_node(){
		total = 0;
		hueristic = 0;
		cost = 0;
		type = empty;
		x = -1;
		y = -1;
	}
	float set_score(){
		total = cost + hueristic;
	}
	void print(){
		if(type == wall){
			std::cout << "|  -  |";
		}else if(type == important){
			std::cout << "|  *  |";
		}else{
			std::cout << "| " << std::setw(3) << total << " |";
		}
	}

	bool operator()(const star_node* left, const star_node* right){
		return (left->total > right->total);
	}
} star_node_obj;

/*
* A 2D graph of type T
*/
template<typename T>
class graph2d{
	T** graph;
public:
	int width;
	int height;

	graph2d(int xsize, int ysize){
		width = xsize;
		height = ysize;
		graph = new T*[width];

		for(int i = 0; i < width; i++)
			graph[i] = new T[height];

		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				graph[i][j].x = i;
				graph[i][j].y = j;
			}
		}
	}
	~graph2d(){
		//for(int i = 0; i < width; i++)
		//	delete[] graph[i];
		//delete[] graph;
	}

	T* get(int x, int y){
		return &graph[x][y];
	}
	void print(){
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++)
				std::cout << "-------";
			std::cout << std::endl;
			for(int j = 0; j < width; j++)
				graph[j][i].print();
			std::cout << std::endl;
			for(int j = 0; j < width; j++)
				std::cout << "-------";
			std::cout << std::endl;
		}
	}
};

/*------------------------------------------------
* open list, force vector to act like heap
* The open list then is just a heap we can search
*-------------------------------------------------*/
inline void set_open_list(std::vector<star_node*>& v){
	std::make_heap(v.begin(), v.end(), star_node_obj);
}

inline star_node* pop_list(std::vector<star_node*>& v){
	std::pop_heap(v.begin() ,v.end(), star_node_obj);
	star_node* temp = v.back();
	v.pop_back();
	std::cout << "temp is " << temp->total << "\n";
	return temp;
}

inline void push_list(std::vector<star_node*>& v, star_node* node){
	v.push_back(node);
	std::push_heap(v.begin(), v.end(), star_node_obj);
}

inline void update_list(std::vector<star_node*>& v){
	std::make_heap(v.begin(), v.end(), star_node_obj);
}

#endif //A_STAR_H